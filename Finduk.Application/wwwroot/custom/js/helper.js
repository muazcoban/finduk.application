﻿const baseUrl = "http://134.209.120.100:8090/";

const MessageType = {
    Success:1,
    Information: 2,
    Warning: 3,
    Error:4
};

const MessageTitle = {
    Success: "Başarılı! ",
    Information: "Bilgi ",
    Warning: "Uyarı ",
    Error: "Hata! "
};

const showStatusMessageWaitingTime = 4000;

const jsGridDefaultOptions = {
    align: "center",
    width: "100%",
    height: "auto",
    loadIndication: true,
    loadIndicationDelay: 250,
    loadMessage: "Lütfen bekleyiniz...",
    loadShading: true,
    updateOnResize: true,
    selecting: true,
    inserting: true,
    editing: true,
    sorting: true,
    confirmDeleting: true,
    heading: true,
    filtering: true,
    paging: true,
    pageLoading: true,
    autoload: true
};

function callAPI(url, data, type) {
    let result;
    var token = "";
    if (loginInfo && loginInfo.token) {
        token = loginInfo.token;
    }
    try {
        $.ajax({
        headers: { "Authorization": "jwt " + token },
        contentType: "application/json",
        accept: "application/json",
        type: type,
        url: url,
        data: data,
        async: false,
        context: this,
        global: false,
        dataType: "json",
        success: function (data) {
            result = data;
        },
        error: function (xhr, err) {
            result = err;
            if (xhr && xhr.responseText) {
                showStatusMessage(url + " İşlem esnasında hata oluştu. " + xhr.status + " - " + xhr.statusText, MessageType.Error);
                console.log(xhr.responseText);
            }
            else {
                showStatusMessage("İşlem esnasında hata oluştu. " + err, MessageType.Error);
            }
        }
    });
    return result;
    } catch (error) {
        showStatusMessage("İşlem esnasında hata oluştu. " + error, MessageType.Error);
    return error;
    }
}

function setTotalInfo(data, prop, tlElement, usdElement, eurElement) {
    if (data) {
        var totalTL = 0;
        var totalUSD = 0;
        var totalEUR = 0;
        data.forEach((item) => {
            if (item[prop]) {
                if (item.currency == 1) {
                    totalUSD += item[prop];
                }
                else if (item.currency == 2) {
                    totalTL += item[prop];
                }
                else if (item.currency == 3) {
                    totalEUR += item[prop];
                }
            }
        })
        if (tlElement) {
            tlElement.append(getCounterObject(totalTL, 'TL', 'icon-lira-sign'));
        }
        if (usdElement) {
            usdElement.append(getCounterObject(totalUSD, 'USD', 'icon-dollar'));
        }
        if (eurElement) {
            eurElement.append(getCounterObject(totalEUR, 'EUR', 'icon-euro-sign'));
        }
    }
}

function getCounterObject(total, description, icon) {
    var div = $('<div class="col_one_fourth nobottommargin center">');
    var i = $('<i class="i-plain i-xlarge divcenter nobottommargin"></i>');
    i.attr("class", icon);
    div.append(i);
    var subDiv = $("<div>");
    subDiv.css("color", "#1abc9c");
    subDiv.attr("class", "counter counter-large");
    var subSpan = $("<span>");
    subSpan.attr('data-from', '0');
    subSpan.attr('data-speed', '2000');
    subSpan.attr('data-refresh-interval', '50');
    subSpan.attr('data-to', total);
    subDiv.append(subSpan);
    div.append(subDiv);
    var h5 = $('<h5></h5>');
    h5.append(total + ' ' + description);
    div.append(h5);
    return div;
}

function showStatusMessage(messageText, messageType) {
    var containerDiv = $('#showStatusMessage');
    if (!containerDiv) {
        containerDiv = $("<div id='showStatusMessage'>")
            .attr("data-notify-position", "bottom-left")
            .attr("data-notify-close", "true");
        $("body").append(containerDiv);
    }

    if (messageType == MessageType.Success) {
        containerDiv
            .attr("data-notify-type", "success")
            .attr("data-notify-msg", "<i class= icon-ok-sign></i> " + messageText);
    }
    else if (messageType == MessageType.Information) {
        containerDiv
            .attr("data-notify-type", "info")
            .attr("data-notify-msg", "<i class= icon-info-sign></i> " + messageText);
    }
    else if (messageType == MessageType.Warning) {
        containerDiv
            .attr("data-notify-type", "warning")
            .attr("data-notify-msg", "<i class= icon-warning-sign></i> " + messageText);
    }
    else if (messageType == MessageType.Error) {
        containerDiv
            .attr("data-notify-type", "error")
            .attr("data-notify-msg", "<i class= icon-remove-sign></i> " + messageText);
    }
    if (typeof SEMICOLON !== 'undefined') {
        SEMICOLON.widget.notifications(jQuery('#showStatusMessage'));
        return false;
    }

}

function showModal(transcationCode, content) {
    var gridContainer = $('#' + transcationCode);
    var modalContainer = $('#modal' + transcationCode);
    if (!document.getElementById('modal' + transcationCode)) {
        modalContainer = $("<div id='modal" + transcationCode + "'>");
        gridContainer.append(modalContainer);
    }

    modalContainer.empty();
    modalContainer.append(content);
    $('#' + transcationCode + 'FormModal').modal('show');
}

jsGrid.loadStrategies.PageLoadingStrategy.prototype.sort = function () {
    this._grid._sortData();
    this._grid.refresh();
    return $.Deferred().resolve().promise();
};

jsGrid.loadStrategies.PageLoadingStrategy.prototype.filter = function () {
    alert("filter");
    this._grid._filter();
    this._grid.refresh();
    return $.Deferred().resolve().promise();
};

var CustomDateField = function (config) {
    jsGrid.Field.call(this, config);
};

CustomDateField.prototype = new jsGrid.Field({
    sorter: function (date1, date2) {
        return new Date(date1) - new Date(date2);
    },

    itemTemplate: function (value) {
        return new Date(value).toDateString();
    },

    insertTemplate: function (value) {
        return this._insertPicker = $("<input>").datepicker({ defaultDate: new Date() });
    },

    editTemplate: function (value) {
        return this._editPicker = $("<input>").datepicker().datepicker("setDate", new Date(value));
    },

    insertValue: function () {
        return this._insertPicker.datepicker("getDate").toISOString();
    },

    editValue: function () {
        return this._editPicker.datepicker("getDate").toISOString();
    }
});

jsGrid.fields.customDateField = CustomDateField;

var CustomDateTimeField = function (config) {
    jsGrid.Field.call(this, config);
};
CustomDateTimeField.prototype = new jsGrid.Field({
    sorter: function (date1, date2) {
        return new Date(date1) - new Date(date2);
    },

    itemTemplate: function (value) {
        return new Date(value).toLocaleString();
    },

    insertTemplate: function (value) {
        return this._insertPicker = $("<input>").attr("class", "form-control datetimepicker-input tleft").attr("type", "text").datetimepicker({ defaultDate: new Date() });
    },

    editTemplate: function (value) {
        return this._editPicker = $("<input>").attr("class", "form-control datetimepicker-input tleft").attr("type", "text").datetimepicker().datepicker("setDate", new Date(value));
    },

    insertValue: function () {
        return this._insertPicker.datetimepicker("getDate").toISOString();
    },

    editValue: function () {
        return this._editPicker.datetimepicker("getDate").toISOString();
    }
});

jsGrid.fields.customDateTimeField = CustomDateTimeField;




var SolRiaDateTimeField = function (config) {
    jsGrid.Field.call(this, config);
};
SolRiaDateTimeField.prototype = new jsGrid.Field({
    sorter: function (date1, date2) {
        return new Date(date1) - new Date(date2);
    },

    itemTemplate: function (value) {
        if (value === null) {
            return '';
        } else {
            return moment(value).format('L LTS');
        }
    },

    insertTemplate: function (value) {
        this._insertPicker = $('<input>').datetimepicker({
            format: 'L LTS',
            defaultDate: moment(),
            widgetPositioning: {
                horizontal: 'auto',
                vertical: 'bottom'
            }
        });

        this._insertPicker.data('DateTimePicker').date(moment());
        return this._insertPicker;
    },

    editTemplate: function (value) {
        this._editPicker = $('<input>').datetimepicker({
            format: 'L LTS',
            widgetPositioning: {
                horizontal: 'auto',
                vertical: 'bottom'
            }
        });

        if (value !== null) {
            this._editPicker.data('DateTimePicker').defaultDate(moment(value));
            this._editPicker.data('DateTimePicker').date(moment(value));
        }
        return this._editPicker;
    },

    insertValue: function () {
        var insertValue = this._insertPicker.data('DateTimePicker').date();
        if (typeof insertDate !== 'undefined' && insertDate !== null) {
            return insertDate.format('L LTS');
        } else {
            return null;
        }
    },

    editValue: function () {
        var editValue = this._editPicker.data('DateTimePicker').date();
        if (typeof editValue !== 'undefined' && editValue !== null) {
            return editValue.format('L LTS');
        } else {
            return null;
        }
    }
});
jsGrid.fields.solRiaDateTimeField = SolRiaDateTimeField;

