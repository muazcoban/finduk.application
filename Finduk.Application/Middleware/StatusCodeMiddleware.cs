﻿using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Finduk.Application.Middleware
{
    public class StatusCodeMiddleware
    {
        private readonly RequestDelegate _nextMiddleware;
        public StatusCodeMiddleware(RequestDelegate next)
        {
            _nextMiddleware = next;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            await _nextMiddleware.Invoke(httpContext);

            if (httpContext.Response.StatusCode == 404)
            {
                await httpContext.Response
                .WriteAsync("Aradığınız sayfa bulunamadı.", Encoding.UTF8);
            }
        }
    }
}
