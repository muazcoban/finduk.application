﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Finduk.Application.Base
{
    public static class Executer
    {
        private static HttpClient client = new HttpClient();
        private static string BaseURL = "http://134.209.120.100:8090/";

        /// <summary>
        /// GetWebResponseAsync
        /// </summary>
        /// <param name="request">License Server URI to retrieve the PlayReady license.</param>
        public static GenericResponse<JToken> Exec(RequestBase request)
        {
            GenericResponse<JToken> response = Common.GetGenericResponse<JToken>();
            try
            {
                using (var httpClient = new HttpClient())
                {
                    httpClient.Timeout = new TimeSpan(0, 5, 0);
                    httpClient.DefaultRequestHeaders.Clear();
                    httpClient.DefaultRequestHeaders.Accept.Clear();
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    if (httpClient.BaseAddress == null)
                    {
                        httpClient.BaseAddress = new Uri(BaseURL);
                    }
                    
                    if (ApplicationContext.LoginInfo != null && !string.IsNullOrEmpty(ApplicationContext.LoginInfo.token))
                    {
                        httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "jwt " + ApplicationContext.LoginInfo.token);
                    }
                    var responseGet = httpClient.GetAsync(request.MethodName + "/").Result;
                    if (responseGet.IsSuccessStatusCode)
                    {
                        var jsonObject = JObject.Parse(responseGet.Content.ReadAsStringAsync().Result);
                        response.Value = jsonObject[!String.IsNullOrEmpty(request.InstanceName) ? request.InstanceName : request.MethodName];
                        response.isSuccess = responseGet.IsSuccessStatusCode;
                    }
                    else
                    {
                        response.isSuccess = false;
                        var result = new Result();
                        result.StatusCode = (int)responseGet.StatusCode;
                        result.StatusText = responseGet.ReasonPhrase;
                        response.ResultList.Add(result);
                    }
                    return response;
                }
            }
            catch (Exception exception)
            {
                return null;
            }
            return response;
        }

        /// <summary>
        /// PostAsync
        /// </summary>
        /// <param name="request">License Server URI to retrieve the PlayReady license.</param>
        public static async Task<GenericResponse<JToken>> PostAsync(StringContent content, string methodName)
        {
            GenericResponse<JToken> returnObject = Common.GetGenericResponse<JToken>();
            string responseBody = String.Empty;
            try
            {
                using (var httpClient = new HttpClient())
                {
                    if (ApplicationContext.LoginInfo != null && !string.IsNullOrEmpty(ApplicationContext.LoginInfo.token))
                    {
                        httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "jwt " + ApplicationContext.LoginInfo.token);
                    }
                    using (var response = await httpClient.PostAsync(BaseURL + methodName + "/", content))
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();
                        if (response.IsSuccessStatusCode)
                        {
                            responseBody = response.Content.ReadAsStringAsync().Result;
                            var jsonObject = JObject.Parse(responseBody);
                            returnObject.Value = jsonObject;
                            returnObject.isSuccess = response.IsSuccessStatusCode;
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                return null;
            }

            return returnObject;
        }

    }
}

