﻿using System;
using Finduk.Application.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Finduk.Application.Base
{
    public class BaseController : Controller
    {
        public BaseController()
        {
        }

        public override void OnActionExecuted(ActionExecutedContext context)
        {
            base.OnActionExecuted(context);
            ApplicationContext.LoginInfo = HttpContext.Session.GetObject<LoginResponseModel>(Common.SessionKey);
        }

        public override ViewResult View()
        {
            return base.View();
        }

        public override PartialViewResult PartialView()
        {
            return base.PartialView();
        }

    }
}
