﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Finduk.Application.Base
{
    public class ModelBase
    {

        #region SelectListItem

        readonly SelectListItem selectItem = new SelectListItem
        {
            Text = "Seçiniz",
            Value = null
        };
        #endregion

        #region StoreListItems

        private List<SelectListItem> storeListItems { get; set; }
        public List<SelectListItem> StoreListItems
        {
            get
            {
                var storeList = ApplicationContext.StoreList;
                storeListItems = new List<SelectListItem>();
                storeListItems.Add(selectItem);
                foreach (var store in storeList)
                {
                    storeListItems.Add(new SelectListItem
                    {
                        Text = store.name,
                        Value = store.id.ToString()
                    });
                }
                return storeListItems;
            }
        }
        #endregion

        #region CompanyListItems

        private List<SelectListItem> companyListItems { get; set; }
        public List<SelectListItem> CompanyListItems
        {
            get
            {
                var companyList = ApplicationContext.CompanyList;
                companyListItems = new List<SelectListItem>();
                companyListItems.Add(selectItem);
                foreach (var company in companyList)
                {
                    companyListItems.Add(new SelectListItem
                    {
                        Text = company.name,
                        Value = company.id.ToString()
                    });
                }
                return companyListItems;
            }
        }
        #endregion

        #region ProductListItems

        private List<SelectListItem> productListItems { get; set; }
        public List<SelectListItem> ProductListItems
        {
            get
            {
                var productList = ApplicationContext.ProductList;
                productListItems = new List<SelectListItem>();
                productListItems.Add(selectItem);
                foreach (var product in productList)
                {
                    productListItems.Add(new SelectListItem
                    {
                        Text = product.name,
                        Value = product.id.ToString()
                    });
                }
                return productListItems;
            }
        }
        #endregion

        #region ContactListItems

        private List<SelectListItem> contactListItems { get; set; }
        public List<SelectListItem> ContactListItems
        {
            get
            {
                var contactList = ApplicationContext.ContactList;
                contactListItems = new List<SelectListItem>();
                contactListItems.Add(selectItem);
                foreach (var contact in contactList)
                {
                    contactListItems.Add(new SelectListItem
                    {
                        Text = contact.first_name + " " + contact.last_name + " -" + contact.title,
                        Value = contact.id.ToString()
                    });
                }
                return contactListItems;
            }
        }
        #endregion

        #region WayBillListItems

        private List<SelectListItem> wayBillListItems { get; set; }
        public List<SelectListItem> WayBillListItems
        {
            get
            {
                var wayBillList = ApplicationContext.WayBillList;
                wayBillListItems = new List<SelectListItem>();
                wayBillListItems.Add(selectItem);
                foreach (var wayBill in wayBillList)
                {
                    wayBillListItems.Add(new SelectListItem
                    {
                        Text = wayBill.waybill_no,
                        Value = wayBill.id.ToString()
                    });
                }
                return wayBillListItems;
            }
        }

        #endregion

        #region WayBillTypeListItems

        private List<SelectListItem> wayBillTypeListItems { get; set; }
        public List<SelectListItem> WayBillTypeListItems
        {
            get
            {
                var wayBillTypeList = ApplicationContext.FinanceType;
                wayBillTypeListItems = new List<SelectListItem>();
                wayBillTypeListItems.Add(selectItem);
                foreach (var wayBillType in wayBillTypeList)
                {
                    wayBillTypeListItems.Add(new SelectListItem
                    {
                        Text = wayBillType.Text,
                        Value = wayBillType.Id.ToString()
                    });
                }
                return wayBillTypeListItems;
            }
        }

        #endregion

        #region QuantityTypeListItems

        private List<SelectListItem> quantityTypeListItems { get; set; }
        public List<SelectListItem> QuantityTypeListItems
        {
            get
            {
                var quantityTypeList = ApplicationContext.QuantityType;
                quantityTypeListItems = new List<SelectListItem>();
                quantityTypeListItems.Add(selectItem);
                foreach (var quantityType in quantityTypeList)
                {
                    quantityTypeListItems.Add(new SelectListItem
                    {
                        Text = quantityType.Text,
                        Value = quantityType.Id.ToString()
                    });
                }
                return quantityTypeListItems;
            }
        }

        #endregion

        #region CurrencyListItems

        private List<SelectListItem> currencyListItems { get; set; }
        public List<SelectListItem> CurrencyListItems
        {
            get
            {
                var currencyList = ApplicationContext.Currency;
                currencyListItems = new List<SelectListItem>();
                currencyListItems.Add(selectItem);

                foreach (var currency in currencyList)
                {
                    currencyListItems.Add(new SelectListItem
                    {
                        Text = currency.Text,
                        Value = currency.Id.ToString()
                    });
                }
                return currencyListItems;
            }
        }

        #endregion

        #region InvoiceTypeListItems

        private List<SelectListItem> invoiceTypeListItems { get; set; }
        public List<SelectListItem> InvoiceTypeListItems
        {
            get
            {
                var invoiceTypeList = ApplicationContext.FinanceType;
                invoiceTypeListItems = new List<SelectListItem>();
                invoiceTypeListItems.Add(selectItem);
                foreach (var invoiceType in invoiceTypeList)
                {
                    invoiceTypeListItems.Add(new SelectListItem
                    {
                        Text = invoiceType.Text,
                        Value = invoiceType.Id.ToString()
                    });
                }
                return invoiceTypeListItems;
            }
        }

        #endregion

        #region ContactUserItems

        private List<SelectListItem> contactUserItems { get; set; }
        public List<SelectListItem> ContactUserItems
        {
            get
            {
                var userList = ApplicationContext.UserList;
                contactUserItems = new List<SelectListItem>();
                contactUserItems.Add(selectItem);
                foreach (var user in userList)
                {
                    contactUserItems.Add(new SelectListItem
                    {
                        Text = user.first_name + ' ' + user.last_name,
                        Value = user.id.ToString()
                    });
                }
                return contactUserItems;
            }

        }

        #endregion

        #region CountryListItems

        private List<SelectListItem> countryListItems { get; set; }
        public List<SelectListItem> CountryListItems
        {
            get
            {
                var countryList = ApplicationContext.CountryList;
                countryListItems = new List<SelectListItem>();
                countryListItems.Add(selectItem);
                foreach (var country in countryList)
                {
                    countryListItems.Add(new SelectListItem
                    {
                        Text = country.name,
                        Value = country.id.ToString()
                    });
                }
                return countryListItems;
            }

        }

        #endregion

        #region CompanyCityListItems

        private List<SelectListItem> companyCityListItems { get; set; }
        public List<SelectListItem> CompanyCityListItems
        {
            get
            {
                var companyCityList = ApplicationContext.CompanyCityList;
                companyCityListItems = new List<SelectListItem>();
                companyCityListItems.Add(selectItem);
                foreach (var city in companyCityList)
                {
                    companyCityListItems.Add(new SelectListItem
                    {
                        Text = city.name,
                        Value = city.id.ToString()
                    });
                }
                return companyCityListItems;
            }

        }

        #endregion
    }
}
