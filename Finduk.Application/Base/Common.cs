﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Finduk.Application.Models;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;

namespace Finduk.Application.Base
{
    
    public static class Common
    {

        public const string SessionKey = "loginToken";
        public static LoginResponseModel GetLoginInfo()
        {
            var loginInfo = ApplicationContext.HttpContext.Session.GetObject<LoginResponseModel>(Common.SessionKey);
            return loginInfo;
        }
        public static GenericResponse<T> GetGenericResponse<T>()
        {
            GenericResponse<T> genericResponse = new GenericResponse<T>();
            return genericResponse;
        }

        public static GenericResponse GetResponse<Response>() where Response : GenericResponse
        {
            Type t = typeof(Response);
            if (t.IsGenericType)
            {
                ConstructorInfo ci = t.GetConstructor(BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] {
                typeof(bool)
            }, null);
                object obj = ci.Invoke(new object[] { true });
                return (Response)obj;
            }
            else
            {
                ConstructorInfo ci = t.GetConstructor(new Type[] { });
                object obj = ci.Invoke(new object[] { });
                return (Response)obj;
            }
        }

        public static List<CountryViewModel> GetCountryList()
        {
            var countryResponse = Executer.Exec(new RequestBase()
            {
                MethodName = ApiMethod.Country
            });
            if (countryResponse.isSuccess)
            {
                return countryResponse.Value.ToObject<List<CountryViewModel>>();
            }
            else
            {
                return null;
            }

        }

        public static List<CityViewModel> GetCompanyCityList()
        {
            if (ApplicationContext.CurrentCountryId > 0)
            {
                var cityResponse = Executer.Exec(new RequestBase()
                {
                    MethodName = string.Format(ApiMethod.GetCity, ApplicationContext.CurrentCountryId.ToString()),
                    InstanceName = ApiMethod.City
                });
                if (cityResponse.isSuccess)
                {
                    return cityResponse.Value.ToObject<List<CityViewModel>>();
                }
            }
            return null;
        }

        public static List<CompanyViewModel> GetCompanyList()
        {
            var companyResponse = Executer.Exec(new RequestBase()
            {
                MethodName = ApiMethod.Company
            });
            if (companyResponse.isSuccess)
            {
                return companyResponse.Value.ToObject<List<CompanyViewModel>>();
            }
            else
            {
                return null;
            }
        }

        public static List<ProductViewModel> GetProductList()
        {
            var productResponse = Executer.Exec(new RequestBase()
            {
                MethodName = ApiMethod.Product
            });
            if (productResponse.isSuccess)
            {
                return productResponse.Value.ToObject<List<ProductViewModel>>();
            }
            else
            {
                return null;
            }
        }

        public static List<ContactViewModel> GetContactList()
        {
            var contactResponse = Executer.Exec(new RequestBase()
            {
                MethodName = ApiMethod.Contact
            });
            if (contactResponse.isSuccess)
            {
                return contactResponse.Value.ToObject<List<ContactViewModel>>();
            }
            else
            {
                return null;
            }
        }

        public static List<StoreViewModel> GetStoreList()
        {
            var storeResponse = Executer.Exec(new RequestBase()
            {
                MethodName = ApiMethod.Store
            });
            if (storeResponse.isSuccess)
            {
                return storeResponse.Value.ToObject<List<StoreViewModel>>();
            }
            else
            {
                return null;
            }
        }

        public static List<UserViewModel> GetUserList()
        {
            var userResponse = Executer.Exec(new RequestBase()
            {
                MethodName = ApiMethod.User
            });
            if (userResponse.isSuccess)
            {
                return userResponse.Value.ToObject<List<UserViewModel>>();
            }
            else
            {
                return null;
            }
        }

        public static List<InvoiceViewModel> GetInvoiceList()
        {
            var invoiceResponse = Executer.Exec(new RequestBase()
            {
                MethodName = ApiMethod.Invoice
            });
            if (invoiceResponse.isSuccess)
            {
                return invoiceResponse.Value.ToObject<List<InvoiceViewModel>>();
            }
            else
            {
                return null;
            }
        }

        public static List<WayBillViewModel> GetWayBillList()
        {
            var wayBillResponse = Executer.Exec(new RequestBase()
            {
                MethodName = ApiMethod.Waybill
            });
            if (wayBillResponse.isSuccess)
            {
                return wayBillResponse.Value.ToObject<List<WayBillViewModel>>();
            }
            else
            {
                return null;
            }
        }
        
        public static void GetStaticData()
        {
            #region Currency
            ApplicationContext.Currency.Add(new KeyValue()
            {
                Text = "USD",
                Id = 1
            });
            ApplicationContext.Currency.Add(new KeyValue()
            {
                Text = "TL",
                Id = 2
            });
            ApplicationContext.Currency.Add(new KeyValue()
            {
                Text = "EUR",
                Id = 3
            });
            #endregion

            #region FinanceType
            ApplicationContext.FinanceType.Add(new KeyValue()
            {
                Text = "Gelen",
                Id = 1
            });
            ApplicationContext.FinanceType.Add(new KeyValue()
            {
                Text = "Giden",
                Id = 2
            });
            #endregion

            #region QuantityType
            ApplicationContext.QuantityType.Add(new KeyValue()
            {
                Text = "Gram",
                Id = 1
            });
            ApplicationContext.QuantityType.Add(new KeyValue()
            {
                Text = "Kilo",
                Id = 2
            });
            ApplicationContext.QuantityType.Add(new KeyValue()
            {
                Text = "Ton",
                Id = 3
            });
            #endregion

            #region FinanceStatus
            ApplicationContext.FinanceStatus.Add(new KeyValue()
            {
                Text = "Aktif",
                Id = 1
            });
            ApplicationContext.FinanceStatus.Add(new KeyValue()
            {
                Text = "Pasif",
                Id = 0
            });
            #endregion


        }

    }

    public static class ApiMethod
    {
        public static string User = "users";
        public static string Language = "languages";
        public static string Country = "countries";
        public static string City = "cities";
        public static string GetCity = "countries/{0}/cities";
        public static string Company = "companies";
        public static string Contact = "contacts";
        public static string Receipt = "receipts";
        public static string Invoice = "invoices";
        public static string Waybill = "waybills";
        public static string Store = "stores"; 
        public static string GeneralFeature = "general-features";
        public static string Product = "products"; 
        public static string DailyPrices = "daily-prices"; 
        public static string Expensives = "expensives"; 
        public static string StoreAction = "store-actions"; 
        public static string CacheFlow = "cash-flows"; 
        public static string Login = "login"; 
        public static string TokenRefresh = "token-refresh"; 

    }
    public class KeyValue
    {
        public string Text { get; set; }
        public int Id { get; set; }
    }
}
