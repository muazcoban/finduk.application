﻿using System;
using System.Collections.Generic;

namespace Finduk.Application.Base
{
    public class RequestBase
    {
        public RequestBase()
        {
        }
        public string MethodName { get; set; }
        public string InstanceName { get; set; }
    }
    public class GenericResponse<T> : GenericResponse
    {
        public T Value { get; set; }

        
    }

    public class GenericResponse
    {
        public GenericResponse()
        {
            ResultList = new List<Result>();
        }

        public bool isSuccess { get; set; }
        public List<Result> ResultList { get; set; }
    }
    public class Result
    {
        public int StatusCode { get; set; }
        public string StatusText { get; set; }
    }

}
