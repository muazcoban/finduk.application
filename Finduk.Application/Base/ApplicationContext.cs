﻿using System;
using System.Collections.Generic;
using System.Web;
using Finduk.Application.Base;
using Finduk.Application.Models;
using Microsoft.AspNetCore.Http;

public sealed class ApplicationContext
{
    private static readonly Lazy<ApplicationContext>
        lazy =
        new Lazy<ApplicationContext>
            (() => new ApplicationContext());

    public static ApplicationContext Instance { get { return lazy.Value; } }
    public static HttpContext HttpContext { get; set; }
    private ApplicationContext()
    {
        
    }
    public static LoginResponseModel LoginInfo { get; set; }
    private static List<CountryViewModel> countryList { get; set; }
    public static List<CountryViewModel> CountryList {
        get
        {
            return Common.GetCountryList();
        }
    }

    private static int currentCountryId { get; set; }
    public static int CurrentCountryId {
        get {
            if (currentCountryId == 0)
            {
                if (CountryList != null && CountryList.Count > 0)
                {
                    currentCountryId = CountryList[0].id;
                }
            }
            return currentCountryId;
        }
    }

    private static List<CityViewModel> companyCityList { get; set; }
    public static List<CityViewModel> CompanyCityList {
        get
        {
            return Common.GetCompanyCityList(); 
        }
    }


    private static List<CompanyViewModel> companyList { get; set; }
    public static List<CompanyViewModel> CompanyList
    {
        get
        {
            return Common.GetCompanyList(); 
        }
    }

    private static List<ProductViewModel> productList { get; set; }
    public static List<ProductViewModel> ProductList
    {
        get
        {
            return Common.GetProductList(); 
        }
    }

    private static List<ContactViewModel> contactList { get; set; }
    public static List<ContactViewModel> ContactList
    {
        get
        {
            return Common.GetContactList(); 
        }
    }

    private static List<StoreViewModel> storeList { get; set; }
    public static List<StoreViewModel> StoreList
    {
        get
        {
            return Common.GetStoreList(); 
        }
    }

    private static List<UserViewModel> userList { get; set; }
    public static List<UserViewModel> UserList
    {
        get
        {
            return Common.GetUserList();
        }
    }

    private static List<InvoiceViewModel> invoiceList { get; set; }
    public static List<InvoiceViewModel> InvoiceList
    {
        get
        {
            return Common.GetInvoiceList();
        }
    }

    private static List<WayBillViewModel> wayBillList { get; set; }
    public static List<WayBillViewModel> WayBillList
    {
        get
        {
            return Common.GetWayBillList();
        }
    }
    
    public static List<KeyValue> Currency = new List<KeyValue>();
    public static List<KeyValue> FinanceType = new List<KeyValue>();
    public static List<KeyValue> QuantityType = new List<KeyValue>();
    public static List<KeyValue> FinanceStatus = new List<KeyValue>();
}

