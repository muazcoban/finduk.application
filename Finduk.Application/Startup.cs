using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Finduk.Application.Base;
using Finduk.Application.Middleware;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Text.Json;
using System.Text.Json.Serialization;
using Finduk.Application.Models;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Localization;

namespace Finduk.Application
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<RequestLocalizationOptions>(options =>
            {
                options.DefaultRequestCulture = new RequestCulture("tr-TR");
            });
            services.AddSingleton<Microsoft.AspNetCore.Http.IHttpContext‌​Accessor, Microsoft.AspNetCore.Http.HttpContextAccessor>();
            services.AddSession(so =>
            {
                so.IdleTimeout = TimeSpan.FromHours(24);
            });
            services.AddControllersWithViews();
        }
        
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            //app.UseMiddleware<StatusCodeMiddleware>();
            //app.UseMvcWithDefaultRoute();
            if (env.IsDevelopment())
            {
                //app.UseDeveloperExceptionPage();
            }
            else
            {
                //app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                //app.UseHsts();
            }
            app.UseSession();
            
            app.UseRequestLocalization();
            app.UseDeveloperExceptionPage();
            //app.UseExceptionHandler("/Home/Error");
            
            //app.UseHttpsRedirection();
            // css ve js kullanımı için gereklidir.
            app.UseStaticFiles();

            app.UseRouting();
            Common.GetStaticData();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "Home",
                    pattern: "Home/{actionName}",
                    defaults: new { controller = "Home", action = "Index" });
                endpoints.MapControllerRoute(
                    name: "Profile",
                    pattern: "Profile/{actionName}",
                    defaults: new { controller = "Profile", action = "Index" });
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Login}/{action=InitialLogin}");
            });
        }

    }
}
