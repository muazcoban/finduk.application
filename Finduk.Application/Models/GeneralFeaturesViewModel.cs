﻿using System;
using Finduk.Application.Base;

namespace Finduk.Application.Models
{
    public class GeneralFeaturesViewModel : ModelBase
    {
        
        public Int32 id { get; set; }
        public CompanyViewModel company { get; set; }
        public int currency { get; set; }
        public int unit_type { get; set; }
        public UserViewModel insert_user { get; set; }
        public DateTime insert_date { get; set; }
    }
}
