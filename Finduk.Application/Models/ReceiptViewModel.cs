﻿using System;
using Finduk.Application.Base;

namespace Finduk.Application.Models
{
    public class ReceiptViewModel:ModelBase
    {
        public Int32 id { get; set; }
        public string receipt_no { get; set; }
        public bool status { get; set; }
        public ProductViewModel product { get; set; }
        public Int32 quantity { get; set; }
        public Int32 quantity_type { get; set; }
        public double price { get; set; }
        public ContactViewModel contact { get; set; }
        public CompanyViewModel company { get; set; }
        public string description { get; set; }
        public string receipt_url { get; set; }
        public DateTime receipt_date { get; set; }
        public UserViewModel insert_user { get; set; }
        public DateTime insert_date { get; set; }
        public StoreViewModel store { get; set; }
        public Int32 receipt_type { get; set; }
       
    }
}
