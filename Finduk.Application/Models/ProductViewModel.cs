﻿using System;
using Finduk.Application.Base;

namespace Finduk.Application.Models
{
    public class ProductViewModel : ModelBase
    {
        public int id { get; set; }
        public string name { get; set; }
        public UserViewModel insert_user { get; set; }
        public DateTime insert_date { get; set; }
        public bool is_tree { get; set; }

    }
    
}
