﻿using System;
using Finduk.Application.Base;

namespace Finduk.Application.Models
{
    public class StoreViewModel:ModelBase
    {
        public Int32 id { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string description { get; set; }
        public UserViewModel insert_user { get; set; }
    }
}
