﻿using System;
using Finduk.Application.Base;

namespace Finduk.Application.Models
{
    public class CompanyViewModel : ModelBase
    {
        public Int32 id { get; set; }
        public string name { get; set; }
        public string phone { get; set; }
        public string fax { get; set; }
        public string tax_office { get; set; }
        public string tax_no { get; set; }
        public string banking_accounts { get; set; }
        public string description { get; set; }
        public string email { get; set; }
        public bool is_active { get; set; }
        public int country_id { get; set; }
        public int city_id { get; set; }
        public CountryViewModel country { get; set; }
        public CityViewModel city { get; set; }
        public UserViewModel insert_user { get; set; }
        public DateTime insert_date { get; set; }
    }
}
