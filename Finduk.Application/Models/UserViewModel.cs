﻿using System;
namespace Finduk.Application.Models
{
    public class UserViewModel
    {
        public int id { get; set; }
        public string username { get; set; }
        public string email { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public DateTime date_joined { get; set; }
        public bool is_superuser { get; set; }
    }
}
