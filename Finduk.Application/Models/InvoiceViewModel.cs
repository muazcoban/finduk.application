﻿using System;
using System.Collections.Generic;
using Finduk.Application.Base;

namespace Finduk.Application.Models
{
    public class InvoiceViewModel:ModelBase
    {
        public Int32 id { get; set; }
        public Int32 invoice_type { get; set; }
        public string invoice_no { get; set; }
        public bool status { get; set; }
        public List<int> waybillList { get; set; }
        public List<WayBillViewModel> waybill { get; set; } 
        public CompanyViewModel company { get; set; }
        public ContactViewModel contact { get; set; }
        public string serial_no { get; set; }
        public string order_no { get; set; }
        public double unit_price { get; set; }
        public Int32 quantity_type { get; set; }
        public double price { get; set; }
        public int currency { get; set; }
        public string description { get; set; }
        public string invoice_url { get; set; }
        public DateTime invoice_date { get; set; }
        public UserViewModel insert_user { get; set; }
        public DateTime insert_date { get; set; }
        public Int32 product { get; set; }
        public double quantity { get; set; }
        public bool e_invoice { get; set; }

    }
}
