﻿using System;
using Finduk.Application.Base;

namespace Finduk.Application.Models
{
    public class CountryViewModel : ModelBase
    {
        public int id { get; set; }
        public string code { get; set; }
        public string language { get; set; }
        public string name { get; set; }
    }
}
