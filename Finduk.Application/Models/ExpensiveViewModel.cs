﻿using System;
using Finduk.Application.Base;

namespace Finduk.Application.Models
{
    public class ExpensiveViewModel:ModelBase
    {
        public Int32 id { get; set; }
        public string description { get; set; }
        public double price { get; set; }
        public int currency { get; set; }
        public DateTime expense_date { get; set; }
        public DateTime insert_date { get; set; }
        public UserViewModel insert_user { get; set; }
    }
}
