﻿using System;
using Finduk.Application.Base;

namespace Finduk.Application.Models
{
    public class DailyPriceViewModel:ModelBase
    {
        public Int32 id { get; set; }
        public ProductViewModel product { get; set; }
        public DateTime daily_date { get; set; }
        public double price { get; set; }
        public Int32 currency { get; set; }
        public UserViewModel insert_user { get; set; }
        public DateTime insert_date { get; set; }
    }
}
