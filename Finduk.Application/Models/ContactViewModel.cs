﻿using System;
using Finduk.Application.Base;

namespace Finduk.Application.Models
{
    public class ContactViewModel : ModelBase
    {
        public Int32 id { get; set; }
        public string title { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public CompanyViewModel company { get; set; }
        public bool is_owner { get; set; }
        public CountryViewModel country { get; set; }
        public CityViewModel city { get; set; }
        public string description { get; set; }
        public bool is_active { get; set; }
        public UserViewModel insert_user { get; set; }
        public DateTime insert_date { get; set; }
        public UserViewModel contact_user { get; set; }
    }
}