﻿using System;
using Finduk.Application.Base;

namespace Finduk.Application.Models
{
    public class CityViewModel : ModelBase
    {
        public int id { get; set; }
        public string name { get; set; }
        public CountryViewModel country { get; set; }
    }
}
