﻿using System;
using System.ComponentModel.DataAnnotations;
using Finduk.Application.Base;

namespace Finduk.Application.Models
{
    public class WayBillViewModel:ModelBase
    {
        public Int32 id { get; set; }
        public string waybill_no { get; set; }
        public string waybill_type { get; set; }
        public string status { get; set; }
        public Int32 contactId { get; set; }
        public ContactViewModel contact { get; set; }
        public Int32 companyId { get; set; }
        public CompanyViewModel company { get; set; }
        public string serial_no { get; set; }
        public string order_no { get; set; }
        public string description { get; set; }
        public string waybill_url { get; set; }
        public DateTime? dispatch_date { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd.MM.yyyy H:mm tt}")]
        public DateTime delivery_date { get; set; }
        public UserViewModel insert_user { get; set; }
        public DateTime insert_date { get; set; }
        public Int32 productId { get; set; }
        public ProductViewModel product { get; set; }
        public int store { get; set; }
        public int quantity { get; set; }
        public int quantity_type { get; set; }
    }
}
