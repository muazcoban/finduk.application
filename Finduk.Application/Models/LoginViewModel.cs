﻿using System;
namespace Finduk.Application.Models
{
    public class LoginViewModel
    {
        public string username { get; set; }
        public string password { get; set; }
    }

    public class LoginResponseModel
    {
        public string token { get; set; }
    }
}
