﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Finduk.Application.Base;
using Finduk.Application.Models;
using Microsoft.AspNetCore.Mvc;


namespace Finduk.Application.Controllers
{
    public class CompanyController : BaseController
    {
        public IActionResult Index()
        {
            ViewBag.PageTitle = "Şirketler";
            ViewBag.PageTitleDescription = "İş yapılan kurumlar";
            ViewBag.NavigationInfo = "Şirketler";
            return View();
        }
        public IActionResult Definition(CompanyViewModel model)
        {
            return PartialView(model);
        }
    }
}
