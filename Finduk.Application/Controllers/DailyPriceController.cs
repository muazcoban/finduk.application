﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Finduk.Application.Base;
using Finduk.Application.Models;
using Microsoft.AspNetCore.Mvc;


namespace Finduk.Application.Controllers
{
    public class DailyPriceController : BaseController
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            ViewBag.PageTitle = "Günlük Fiyat";
            ViewBag.PageTitleDescription = "Günlük Fiyatların listesi";
            ViewBag.NavigationInfo = "Günlük Fiyat";
            return View();
        }

        public IActionResult Definition(DailyPriceViewModel model)
        {
            return PartialView(model);
        }
    }
}
