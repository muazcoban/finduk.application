﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Finduk.Application.Base;
using Finduk.Application.Models;
using Microsoft.AspNetCore.Mvc;


namespace Finduk.Application.Controllers
{
    public class ReceiptController : BaseController
    {
        public IActionResult Index()
        {
            ViewBag.PageTitle = "Makbuz";
            ViewBag.PageTitleDescription = "Makbuz kesimi";
            ViewBag.NavigationInfo = "Makbuz";
            return View();
        }

        public IActionResult Definition(ReceiptViewModel model)
        {
            return PartialView(model);
        }
    }
}
