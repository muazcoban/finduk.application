﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Finduk.Application.Base;
using Finduk.Application.Models;
using Microsoft.AspNetCore.Mvc;

namespace Finduk.Application.Controllers
{
    public class StoreActionController : BaseController
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            ViewBag.PageTitle = "Depo Hareketleri";
            ViewBag.PageTitleDescription = "Tüm depolardaki işlemleri özeti";
            ViewBag.NavigationInfo = "Depo Hareketleri";
            return View();
        }
    }
}
