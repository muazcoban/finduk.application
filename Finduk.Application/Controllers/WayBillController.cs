﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Finduk.Application.Base;
using Finduk.Application.Models;
using Microsoft.AspNetCore.Mvc;

namespace Finduk.Application.Controllers
{
    public class WayBillController : BaseController
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            ViewBag.PageTitle = "İrsaliye";
            ViewBag.PageTitleDescription = "Çıkış yada Giriş bilgileri";
            ViewBag.NavigationInfo = "İrsaliye";
            return View();
        }
        public IActionResult Definition(WayBillViewModel model)
        {
            return PartialView(model);
        }
    }
}
