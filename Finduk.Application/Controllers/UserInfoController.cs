﻿
using Finduk.Application.Base;
using Finduk.Application.Models;
using Microsoft.AspNetCore.Mvc;
namespace Finduk.Application.Controllers
{
    public partial class HomeController
    {
        public IActionResult UserInfo()
        {
            ViewBag.PageTitle = "Kullanıcı Ayarları";
            ViewBag.PageTitleDescription = "Kullanıcı Ayarları";
            ViewBag.NavigationInfo = "Kullanıcılar";
            return View();
        }
    }
}
