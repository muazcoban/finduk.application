﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Finduk.Application.Base;
using Finduk.Application.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Finduk.Application.Controllers
{
    public class StoreController : BaseController
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            ViewBag.PageTitle = "Depo";
            ViewBag.PageTitleDescription = "Ürünlerin saklanma bilgileri";
            ViewBag.NavigationInfo = "Depolar";
            return View();
        }
        public IActionResult Definition(StoreViewModel model)
        {
            return PartialView(model);
        }
    }
}
