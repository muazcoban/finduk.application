﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Finduk.Application.Base;
using Finduk.Application.Models;
using Microsoft.AspNetCore.Mvc;

namespace Finduk.Application.Controllers
{
    public class CacheFlowController : BaseController
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            ViewBag.PageTitle = "Nakit";
            ViewBag.PageTitleDescription = "Nakit Akışı";
            ViewBag.NavigationInfo = "Nakit";
            return View();
        }

    }
}
