﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Finduk.Application.Base;
using Finduk.Application.Models;
using Microsoft.AspNetCore.Mvc;


namespace Finduk.Application.Controllers
{
    public class ExpensiveController : BaseController
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            ViewBag.PageTitle = "Masraf";
            ViewBag.PageTitleDescription = "Masraf bilgisi";
            ViewBag.NavigationInfo = "Masraf";
            return View();
        }

        public IActionResult Definition(ExpensiveViewModel model)
        {
            return PartialView(model);
        }
    }
}
