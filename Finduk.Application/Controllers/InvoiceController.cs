﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Finduk.Application.Base;
using Finduk.Application.Models;
using Microsoft.AspNetCore.Mvc;


namespace Finduk.Application.Controllers
{
    public class InvoiceController : BaseController
    {

        // GET: /<controller>/
        public IActionResult Index()
        {
            ViewBag.PageTitle = "Fatura";
            ViewBag.PageTitleDescription = "Fatura kesimi";
            ViewBag.NavigationInfo = "Fatura";
            return View();
        }

        public IActionResult Definition(InvoiceViewModel model)
        {
            return PartialView(model);
        }

        public IActionResult Detail(int Id)
        {
            ViewBag.PageTitle = "Fatura Detayı";
            ViewBag.PageTitleDescription = "Fatura detay bilgisi";
            ViewBag.NavigationInfo = "Fatura";
            return View();
        }
        [HttpPost]
        public IActionResult InvoiceDetail(int Id)
        {
            return RedirectToAction("Detail", Id);
        }
    }
}
