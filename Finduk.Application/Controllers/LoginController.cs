﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Finduk.Application.Base;
using Finduk.Application.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Finduk.Application.Controllers
{
    public class LoginController : Controller
    {
        public IActionResult InitialLogin()
        {
            var loginInfo = HttpContext.Session.GetObject<LoginResponseModel>(Common.SessionKey);
            if (loginInfo != null && !string.IsNullOrEmpty(loginInfo.token))
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return View();
            }
        }
        [HttpPost]
        public async Task<IActionResult> InitialLoginAsync(LoginViewModel model)
        {
            ApplicationContext.HttpContext = HttpContext;
            var loginInfo = HttpContext.Session.GetObject<LoginResponseModel>(Common.SessionKey);
            if (loginInfo == null )
            {
                StringContent content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                var loginResponse = await Executer.PostAsync(content, ApiMethod.Login);
                if (loginResponse.isSuccess)
                {
                    var loginResponseModel = loginResponse.Value.ToObject<LoginResponseModel>();
                    HttpContext.Session.SetObject(Common.SessionKey, loginResponseModel);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    return View(model);
                }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }

        }

        public IActionResult Logout()
        {
            HttpContext.Session.SetObject(Common.SessionKey, null);
            return RedirectToAction("InitialLogin");
        }
    }
}
