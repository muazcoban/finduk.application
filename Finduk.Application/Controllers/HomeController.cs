﻿
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Finduk.Application.Models;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Finduk.Application.Base;

namespace Finduk.Application.Controllers
{
    public partial class HomeController : BaseController
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            var loginInfo = HttpContext.Session.GetObject<LoginResponseModel>(Common.SessionKey);
            if (loginInfo == null)
            {
                return RedirectToAction("InitialLogin", "Login");
            }
            ViewBag.LoginInfo = loginInfo; 
            return View();
        }

        public IActionResult Contact()
        {
            ViewBag.PageTitle = "Kullanıcı Ayarları";
            ViewBag.PageTitleDescription = "Kullanıcı Ayarları";
            ViewBag.NavigationInfo = "Kullanıcılar";
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            var statusFeature = HttpContext.Features.Get<IStatusCodeReExecuteFeature>();
            if (statusFeature != null)
            {
                _logger.LogCritical("handled 404 for url: {OriginalPath}", statusFeature.OriginalPath);
            }
            var model = new ErrorViewModel
            {
                RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier,
                StatusCode = HttpContext.Response.StatusCode,
            };
            return View(model);
        }
    }
}
