﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Finduk.Application.Base;
using Finduk.Application.Models;
using Microsoft.AspNetCore.Mvc;

namespace Finduk.Application.Controllers
{
    public class GeneralFeaturesController : BaseController
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            ViewBag.PageTitle = "Genel özellikler";
            ViewBag.PageTitleDescription = "Genel özellikler düzenlenir";
            ViewBag.NavigationInfo = "Genel özellikler";
            return View();
        }

        public IActionResult Definition(GeneralFeaturesViewModel model)
        {
            return PartialView(model);
        }
    }
}
