﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Finduk.Application.Models;
using Finduk.Application.Base;
namespace Finduk.Application.Controllers
{
    public class ContactController : BaseController
    {
        public IActionResult Index()
        {
            ViewBag.PageTitle = "Kişiler";
            ViewBag.PageTitleDescription = "İş yapılan kişi yada kurumlar";
            ViewBag.NavigationInfo = "Kişiler";
            return View();
        }
        public IActionResult Definition(ContactViewModel model)
        {
            return PartialView(model);
        }
    }
}
