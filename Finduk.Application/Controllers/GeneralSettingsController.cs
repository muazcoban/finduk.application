﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Finduk.Application.Models;
using Finduk.Application.Base;
namespace Finduk.Application.Controllers
{
    public partial class HomeController
    {
        public IActionResult GeneralSettings()
        {
            ViewBag.PageTitle = "Sistem Ayarları";
            ViewBag.PageTitleDescription = "Dil Ülke ayarları düzenlemesi";
            ViewBag.NavigationInfo = "Sistem Ayarları";
            return View();
        }

        public IActionResult CompanyDefinition(CompanyViewModel model)
        {
            
            return PartialView(model);
        }
    }
}
